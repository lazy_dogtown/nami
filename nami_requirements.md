
# NAMI - Nginx Advanced Management Interface

## LB-Module

- have upstream_endpoints configured for fallback via upstream {}
- use upstream_based loadbalancing / config
- make module for upstream_vars available and dynamically changeable during runtime within each nginx-instance (shared dict)
- manipulate upstream_vars during runtime through a set uris and lua_snippets
- management-urls /nami/$url -> takes / returns json
    - / -> /status/
    - /status/ (GET)
        - list all endpoints with status (up/down/backup) 
        - list all endpoints with config (weight, timeout, max_fails)
        - list runtime_vars for each endpoint (nr of requests, request_bytes) 
        - list status_id 
    - /manage/ (POST)
        - change status of servers (up/down/backup)
        - change config (weight, fail_timeout etc)
        - add new servers 
    - /ping/ 
        - healtcheck? 
    - /check/ 
        - a simple ascii-page as summary to be browsed locally? (similar to stub/status-module)

- Lua-API-Vars:
    - upstream=name -> upstream - name
    - backup = 1/0 -> on/off
    - up = 1/0  -> on/off
    - server = server_adress -> endpoint, from upstream {}
    - weight = number
    - max_fails = number
    - fail_timeout = time
    - sreq = nr -> number of requests taken from this endpoint
    - sbytes = nr -> number of bytes served by this endpount


## interface 


- flask-based webinterface to manipulate multiple upstream_config for various nginx-instances 
- datastore: redis-backend
- might be separated from nginx or on the same server
- get and display status via /nami/status/ from selected nginx-instance
- orders status-changes via /nami/manage/
- reset to default/fallback-conf?

- worker (daemon-like routines)
    - polls upstream_status of each instance (every 5 seconds)
    - if status_id != actual status_id -> order new status; usefull in case of a server-restart with fallback-conf 
    - creates messages (mail, nagios) on each wanted/unwanted upstream_status_change



## links and inspirations

- nginx dynamic upstream config: https://github.com/iZephyr/nginx-http-touch
- nginx plus: http://nginx.org/en/docs/http/ngx_http_upstream_module.html#upstream_conf
- nginx health_check: http://nginx.org/en/docs/http/ngx_http_upstream_module.html#health_check

